show schemas;
create database if not exists hotel_network_db;
-- drop database if exists hotel_network_db;

use hotel_network_db;

show tables;

-- drop table if exists country;
-- drop table if exists city;
-- drop table if exists hotel_network;
-- drop table if exists hotel;
-- drop table if exists appartment;
-- drop table if exists user;
-- drop table if exists reservation;
-- drop table if exists revies;

create table if not exists country(
	id smallint primary key auto_increment,
	code char(3) not null,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists city(
	id smallint primary key auto_increment,
    name varchar(50) not null,
    country_id smallint not null
 --    foreign key (country_id) references country (id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists hotel_network(
	id smallint primary key auto_increment,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists hotel(
	id smallint primary key auto_increment,
    name varchar(50) not null,
    quantity_apartments smallint,
    hotelStar enum('3', '4', '5'),
    street varchar(50) not null,
    numbBuilding varchar(5) not null,
    hotel_network_id smallint not null,
    city_id smallint not null
 --    foreign key (hotel_network_id) references hotel_network (id) on update cascade on delete restrict,
 --    foreign key (city_id) references city (id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists apartment(
	id smallint primary key auto_increment,
	quantity_rooms tinyint,
	quantity_seats tinyint,
	class_type enum('luxury', 'standard', 'economy'),
    hotel_id smallint not null
--     foreign key (hotel_id) references hotel (id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists user(
	id int primary key auto_increment,
    name varchar(30) not null,
    surname varchar(30) not null,
    gender enum('man', 'woman'),
    email varchar(50) not null,
    phone_number char(10),
    number_card char(16)
) engine=InnoDB;

create table if not exists reservation(
	id int primary key auto_increment,
	start_date date,
    finish_date date,
    apartment_id smallint not null,
    user_id int not null
--     foreign key (apartment_id) references apartment (id) on update cascade on delete restrict,
--     foreign key (user_id) references user(id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists review(
	id int primary key auto_increment,
    apartment_id smallint not null,
    user_id int not null
    -- foreign key (apartment_id) references apartment (id) on update cascade on delete restrict,
    -- foreign key (user_id) references user (id) on update cascade on delete restrict
) engine=InnoDB;

#=============================================
alter table city add 
	constraint fk_country_city foreign key (country_id) 
    references country (id) 
on update cascade on delete restrict;

alter table hotel add 
	constraint fk_hotel_hotel_network foreign key (hotel_network_id) 
    references hotel_network (id) 
on update cascade on delete restrict;

alter table hotel add 
	constraint fk_hotel_city foreign key (city_id) 
    references city (id) 
on update cascade on delete restrict;

alter table apartment add
	constraint fk_apartment_hotel foreign key (hotel_id)
    references hotel (id) 
on update cascade on delete restrict;

alter table reservation add 
	constraint fk_reservation_apartment foreign key (apartment_id)
    references apartment (id)
on update cascade on delete restrict;

alter table reservation add 
	constraint fk_reservation_user foreign key (user_id) 
	references user (id) 
on update cascade on delete restrict;

alter table review add 
	constraint fk_review_apartment foreign key (apartment_id)
    references apartment (id)
on update cascade on delete restrict;

alter table review add 
	constraint fk_review_user foreign key (user_id) 
	references user (id) 
on update cascade on delete restrict;
#=============================================
#INSERT
#=============================================
insert into country (id, code, name) values 
	(1, 'UKR', 'Ukrainian'),
    (2, 'POL', 'Poland');

insert into city (id, name, country_id) values 
	(1, 'Kyiv', 1),
	(2, 'Lviv', 1),
	(3, 'Odesa', 1),
	(4, 'Warsaw', 2),
	(5, 'Poznan', 2);

insert into hotel_network (id, name) values 
	(1, 'ukr_hotels'),
	(2, 'pol_hotels');

insert into hotel (id, name, quantity_apartments, hotelStar, street, numbBuilding, hotel_network_id, city_id) values
	(1, 'Grand hotel', 110, '5', 'Svobody Ave', 13, 1, 2),
    (2, 'Leopolis', 90, '5', 'Teatralna Street', 16, 1, 2),
    (3, 'Swiss-hotel', 65, '4', 'Kniazia Romana Street', 20, 1, 2);

insert into apartment (id, quantity_rooms, quantity_seats, class_type, hotel_id) values
	#Grand hotel:
    (1, 2, 3, 'luxury', 1),
	(2, 3, 3, 'luxury', 1),
	(3, 2, 4, 'standard', 1),
    (4, 1, 2, 'standard', 1),
    #Leopolis:
    (5, 2, 2, 'luxury', 2),
    (6, 1, 3, 'economy', 2),
    #Swiss-hotel:
    (7, 2, 2, 'luxury', 3),
    (8, 1, 1, 'economy', 3),
    (9, 2, 2, 'standard', 3);

insert into user (id, name, surname, gender, email, phone_number, number_card) values
	(1, 'Bob', 'Dilan', 'man', 'bob@gmail.com', '0677900439', '1111222233335555'),
    (2, 'Max', 'Anderson', 'man', 'max@gmail.com', '0677911439', '2222555588889999'),
    (3, 'Anna', 'Svong', 'woman', 'anna@gmail.com', '0677911000', '5555000088883333'),
    (4, 'Olha', 'Ivanova', 'woman', 'olha@gmail.com', '0507988145', '0000999955551111');

insert into review (id, apartment_id, user_id) values
	(1, 2, 1),
	(2, 5, 2),
	(3, 6, 4),
	(4, 4, 3),
	(5, 7, 1),
	(6, 5, 1),
	(7, 2, 4);

insert into reservation (id, start_date, finish_date, apartment_id, user_id) values
	(1, '2019-05-15', '2019-05-19', 2, 1),
	(2, '2019-08-22', '2019-09-10', 5, 2),
	(3, '2019-03-18', '2019-03-23', 6, 4),
	(4, '2019-09-25', '2019-09-30', 4, 3),
	(5, '2019-10-02', '2019-10-08', 7, 1),
	(6, '2019-09-15', '2019-09-21', 5, 1),
	(7, '2019-09-19', '2019-09-22', 2, 4);











