package com.epam.model.sevices;

import com.epam.model.dao.implementation.ReservationDAOImpl;
import com.epam.model.dao.implementation.ReviewDAOImpl;
import com.epam.model.entity.Reservation;
import com.epam.model.entity.Review;

import java.sql.SQLException;
import java.util.List;

public class ReservationService {
    public List<Reservation> findAll() throws SQLException {
        return new ReservationDAOImpl().findAll();
    }

    public Reservation findById(Integer id) throws SQLException {
        return new ReservationDAOImpl().findById(id);
    }

    public int create(Reservation entity) throws SQLException {
        return new ReservationDAOImpl().create(entity);
    }

    public int update(Reservation entity) throws SQLException {
        return new ReservationDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new ReservationDAOImpl().delete(id);
    }
}
