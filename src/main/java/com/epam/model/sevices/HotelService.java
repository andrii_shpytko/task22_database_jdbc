package com.epam.model.sevices;

import com.epam.model.dao.implementation.HotelDAOImpl;
import com.epam.model.entity.Hotel;

import java.sql.SQLException;
import java.util.List;

public class HotelService {
    public List<Hotel> findAll() throws SQLException {
        return new HotelDAOImpl().findAll();
    }

    public Hotel findById(Integer id) throws SQLException {
        return new HotelDAOImpl().findById(id);
    }

    public int create(Hotel entity) throws SQLException {
        return new HotelDAOImpl().create(entity);
    }

    public int update(Hotel entity) throws SQLException {
        return new HotelDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new HotelDAOImpl().delete(id);
    }

}
