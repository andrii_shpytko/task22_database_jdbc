package com.epam.model.sevices;

import com.epam.model.dao.implementation.ApartmentDAOImpl;
import com.epam.model.entity.Apartment;

import java.sql.SQLException;
import java.util.List;

public class ApartmentService {
    public List<Apartment> findAll() throws SQLException {
        return new ApartmentDAOImpl().findAll();
    }

    public List<Apartment> findByClassType(String classType) throws SQLException {
        return new ApartmentDAOImpl().findByClassType(classType);
    }

    public Apartment findById(Integer id) throws SQLException {
        return new ApartmentDAOImpl().findById(id);
    }

    public int create(Apartment entity)throws SQLException{
        return new ApartmentDAOImpl().create(entity);
    }

    public int update (Apartment entity)throws SQLException{
        return new ApartmentDAOImpl().update(entity);
    }

    public int delete(Integer id)throws SQLException{
        return new ApartmentDAOImpl().delete(id);
    }
}
