package com.epam.model.sevices;

import com.epam.model.dao.implementation.HotelNetworkDAOImpl;
import com.epam.model.entity.HotelNetwork;

import java.sql.SQLException;
import java.util.List;

public class HotelNetworkService {
    public List<HotelNetwork> findAll() throws SQLException {
        return new HotelNetworkDAOImpl().findAll();
    }

    public HotelNetwork findById(Integer id) throws SQLException {
        return new HotelNetworkDAOImpl().findById(id);
    }

    public int create(HotelNetwork entity) throws SQLException {
        return new HotelNetworkDAOImpl().create(entity);
    }

    public int update(HotelNetwork entity) throws SQLException {
        return new HotelNetworkDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new HotelNetworkDAOImpl().delete(id);
    }


}
