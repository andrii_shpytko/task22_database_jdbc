package com.epam.model.sevices;

import com.epam.model.dao.implementation.ReviewDAOImpl;
import com.epam.model.entity.Review;

import java.sql.SQLException;
import java.util.List;

public class ReviewService {
    public List<Review> findAll() throws SQLException {
        return new ReviewDAOImpl().findAll();
    }

    public Review findById(Integer id) throws SQLException {
        return new ReviewDAOImpl().findById(id);
    }

    public int create(Review entity) throws SQLException {
        return new ReviewDAOImpl().create(entity);
    }

    public int update(Review entity) throws SQLException {
        return new ReviewDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new ReviewDAOImpl().delete(id);
    }

}
