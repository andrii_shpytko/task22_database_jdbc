package com.epam.model.entity.entityType;

public enum Star {

    THREE("3"), FOUR("4"), FIVE("5");

    private String star;

    private Star(String star) {
        this.star = star;
    }

    public String getStar() {
        return star;
    }
}
