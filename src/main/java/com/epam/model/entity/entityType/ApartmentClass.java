package com.epam.model.entity.entityType;

public enum ApartmentClass {
    LUXURY,
    STANDARD,
    ECONOMY
}
