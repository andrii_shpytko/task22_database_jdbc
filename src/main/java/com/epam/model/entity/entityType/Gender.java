package com.epam.model.entity.entityType;

public enum Gender {
    MAN,
    WOMAN
}
