package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "city")
public class City {
    @PrimaryKey
    @Column(name = "id")
    private short id;
    @Column(name = "name", length = 50)
    private String name;
    @Column(name = "country_id")
    private short countryId;

    public City() {
    }

    public City(short id, String name, short countryId) {
        this.id = id;
        this.name = name;
        this.countryId = countryId;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getCountryId() {
        return countryId;
    }

    public void setCountryId(short countryId) {
        this.countryId = countryId;
    }

    @Override
    public String toString() {
        return String.format("%-10d %-20s %-10d", id, name, countryId);
    }
}
