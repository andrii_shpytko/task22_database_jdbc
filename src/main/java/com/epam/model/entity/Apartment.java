package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;
import com.epam.model.entity.entityType.ApartmentClass;

@Table(name = "apartment")
public class Apartment {
    @PrimaryKey
    @Column(name = "id")
    private short id;
    @Column(name = "quantity_rooms")
    private byte quantityRooms;
    @Column(name = "quantity_seats")
    private byte quantitySeats;
    @Column(name = "class_type")
    private ApartmentClass apartmentClass;
    @Column(name = "hotel_id")
    private int hotelId;

    public Apartment() {
    }

    public Apartment(short id, byte quantityRooms, byte quantitySeats,
                     ApartmentClass apartmentClass, int hotelId) {
        this.id = id;
        this.quantityRooms = quantityRooms;
        this.quantitySeats = quantitySeats;
        this.apartmentClass = apartmentClass;
        this.hotelId = hotelId;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public byte getQuantityRooms() {
        return quantityRooms;
    }

    public void setQuantityRooms(byte quantityRooms) {
        this.quantityRooms = quantityRooms;
    }

    public byte getQuantitySeats() {
        return quantitySeats;
    }

    public void setQuantitySeats(byte quantitySeats) {
        this.quantitySeats = quantitySeats;
    }

    public ApartmentClass getApartmentClass() {
        return apartmentClass;
    }

    public void setApartmentClass(ApartmentClass apartmentClass) {
        this.apartmentClass = apartmentClass;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    @Override
    public String toString() {
        return String.format("%-10d %-10d %-10d %-20s %-10d",
                id, quantityRooms, quantitySeats, apartmentClass, hotelId);

    }
}
