package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "country")
public class Country {
    @PrimaryKey
    @Column(name = "id")
    private short id;
    @Column(name = "code", length = 3)
    private String code;
    @Column(name = "name", length = 50)
    private String name;

    public Country() {
    }

    public Country(short id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-10d %-20s %-20s", id, code, name);
    }
}
