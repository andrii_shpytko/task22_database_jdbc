package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;
import com.epam.model.entity.entityType.Star;

@Table(name = "hotel")
public class Hotel {
    @PrimaryKey
    @Column(name = "id")
    private short id;
    @Column(name = "name", length = 50)
    private String name;
    @Column(name = "quantity_apartments")
    private short quantityApartments;
    @Column(name = "hotelStar")
    private Star hotelStar;
    @Column(name = "street", length = 50)
    private String street;
    @Column(name = "numbBuilding", length = 5)
    private String numbBuilding;
    @Column(name = "hotel_network_id")
    private short hotelNetworkId;
    @Column(name = "city_id")
    private short cityId;

    public Hotel() {
    }

    public Hotel(short id, String name, short quantityApartments, Star hotelStar, String street,
                 String numbBuilding, short hotelNetworkId, short cityId) {
        this.id = id;
        this.name = name;
        this.quantityApartments = quantityApartments;
        this.hotelStar = hotelStar;
        this.street = street;
        this.numbBuilding = numbBuilding;
        this.hotelNetworkId = hotelNetworkId;
        this.cityId = cityId;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getQuantityApartments() {
        return quantityApartments;
    }

    public void setQuantityApartments(short quantityApartments) {
        this.quantityApartments = quantityApartments;
    }

    public Star getHotelStar() {
        return hotelStar;
    }

    public void setHotelStar(Star star) {
        this.hotelStar = star;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumbBuilding() {
        return numbBuilding;
    }

    public void setNumbBuilding(String numbBuilding) {
        this.numbBuilding = numbBuilding;
    }

    public short getHotelNetworkId() {
        return hotelNetworkId;
    }

    public void setHotelNetworkId(short hotelNetworkId) {
        this.hotelNetworkId = hotelNetworkId;
    }

    public short getCityId() {
        return cityId;
    }

    public void setCityId(short cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return String.format("%-10d %-20s %-10d %-10s %-20s %-20s %-10d %-10d",
                id, name, quantityApartments, hotelStar, street, numbBuilding, hotelNetworkId, cityId);
    }
}
