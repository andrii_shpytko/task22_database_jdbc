package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "hotel_network")
public class HotelNetwork {
    @PrimaryKey
    @Column(name = "id")
    private short id;
    @Column(name = "name", length = 50)
    private String name;

    public HotelNetwork() {
    }

    public HotelNetwork(short id, String name) {
        this.id = id;
        this.name = name;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-10d %-20s", id, name);
    }
}
