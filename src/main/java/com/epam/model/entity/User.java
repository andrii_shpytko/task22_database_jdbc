package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;
import com.epam.model.entity.entityType.Gender;

@Table(name = "user")
public class User {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 30)
    private String name;
    @Column(name = "surname", length = 30)
    private String surname;
    @Column(name = "gender")
    private Gender gender;
    @Column(name = "email", length = 50)
    private String email;
    @Column(name = "phone_number", length = 10)
    private String phoneNumber;
    @Column(name = "number_card", length = 16)
    private String numberCard;

    public User() {
    }

    public User(int id, String name, String surname, Gender gender,
                String email, String phoneNumber, String numberCard) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.numberCard = numberCard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(String numberCard) {
        this.numberCard = numberCard;
    }
    @Override
    public String toString() {
        return String.format("%-10d %-20s %-20s %-20s %-20s %-20s %-20s",
                id, name, surname, gender, email, phoneNumber, numberCard);
    }
}
