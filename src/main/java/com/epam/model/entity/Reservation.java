package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "reservation")
public class Reservation {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "finish_date")
    private LocalDateTime finishDate;
    @Column(name    = "apartment_id")
    private short apartmentId;
    @Column(name = "user_id")
    private int userId;

    public Reservation() {
    }

    public Reservation(int id, LocalDateTime startDate, LocalDateTime finishDate,
                       short apartmentId, int userId) {
        this.id = id;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.apartmentId = apartmentId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
        this.finishDate = finishDate;
    }

    public short getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(short apartmentId) {
        this.apartmentId = apartmentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    @Override
    public String toString() {
        //todo make this method
        return String.format("%-10d %-10d %-10d", id,startDate, finishDate, apartmentId, userId);
    }
}