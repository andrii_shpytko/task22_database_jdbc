package com.epam.model.entity;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "review")
public class Review {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "apartment_id")
    private short apartmentId;
    @Column(name = "user_id")
    private int userId;

    public Review() {
    }

    public Review(int id, short apartmentId, int userId) {
        this.id = id;
        this.apartmentId = apartmentId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(short apartmentId) {
        this.apartmentId = apartmentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return String.format("%-10d %-10d %-10d", id,apartmentId, userId );
    }
}
