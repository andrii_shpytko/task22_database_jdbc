package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.HotelNetwork;

public interface HotelNetworkDAO extends GeneralDAO<HotelNetwork, Integer> {
}
