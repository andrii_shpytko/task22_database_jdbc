package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.UserDAO;
import com.epam.model.entity.User;
import com.epam.model.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {
    private static final String FIND_ALL = "SELECT * FROM user";
    private static final String FIND_BY_ID = "SELECT * FROM user WHERE id=?";
    private static final String DELETE = "DELETE FROM user WHERE id=?";
    private static final String CREATE = "INSERT INTO user " +
            "(id, name, surname, gender, email, phone_number, number_card) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE user SET name=?, surname=?, gender=?, email=?, " +
            "phone_number=?, number_card=? " +
            "WHERE id=?";

    @Override
    public List<User> findAll() throws SQLException {
        List<User> users = new ArrayList<>();
        try (Statement statement = ConnectionManager.getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    users.add((User) new Transformer(User.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return users;
    }

    @Override
    public User findById(Number id) throws SQLException {
        User entity = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, (Integer) id);
            try (ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    entity = (User) new Transformer(User.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(User entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getSurname());
            preparedStatement.setString(4, String.valueOf(entity.getGender()));
            preparedStatement.setString(5, entity.getEmail());
            preparedStatement.setString(6, entity.getPhoneNumber());
            preparedStatement.setString(7, entity.getNumberCard());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(User entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSurname());
            preparedStatement.setString(3, entity.getGender().name());
            preparedStatement.setString(4, entity.getEmail());
            preparedStatement.setString(5, entity.getPhoneNumber());
            preparedStatement.setString(6, entity.getNumberCard());
            preparedStatement.setInt(7, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
