package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.ReviewDAO;
import com.epam.model.entity.Review;
import com.epam.model.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReviewDAOImpl implements ReviewDAO {
    private static final String FIND_ALL = "SELECT * FROM review";
    private static final String FIND_BY_ID = "SELECT * FROM review WHERE id=?";
    private static final String DELETE = "DELETE FROM review WHERE id=?";
    private static final String CREATE = "INSERT INTO review " +
            "(id, apartment_id, user_id) " +
            "VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE review SET apartment_id=?, user_id=?" +
            "WHERE id=?";

    @Override
    public List<Review> findAll() throws SQLException {
        List<Review> reviews = new ArrayList<>();
        try (Statement statement = ConnectionManager.getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()){
                    reviews.add((Review) new Transformer(Review.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return reviews;
    }

    @Override
    public Review findById(Number number) throws SQLException {
        Review entity = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(FIND_BY_ID)){
            try (ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    entity = (Review) new Transformer(Review.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Review entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(CREATE)){
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setShort(2, entity.getApartmentId());
            preparedStatement.setInt(3, entity.getUserId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Review entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(UPDATE)){
            preparedStatement.setShort(1, entity.getApartmentId());
            preparedStatement.setInt(2, entity.getUserId());
            preparedStatement.setInt(3, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(DELETE)){
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
