package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.CountryDAO;
import com.epam.model.entity.Country;
import com.epam.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CountryDAOImpl implements CountryDAO {
    private static final String FIND_ALL = "SELECT * FROM country";
    private static final String FIND_BY_ID = "SELECT * FROM country WHERE id=?";
    private static final String FIND_BY_NAME = "SELECT * FROM country WHERE name=?";
    private static final String DELETE = "DELETE FROM country WHERE id=?";
    private static final String CREATE = "INSERT INTO country (id, code, name) VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE country SET code=?, name=? WHERE id=?";


    @Override
    public Country findByName(String name) throws SQLException {
        Country country = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_NAME)) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    country = (Country) new Transformer(Country.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return country;
    }

    @Override
    public List<Country> findAll() throws SQLException {
        List<Country> countries = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    countries.add((Country) new Transformer(Country.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return countries;
    }

    @Override
    public Country findById(Number id) throws SQLException {
        Country country = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)) {
            preparedStatement.setShort(1, (Short) id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    country = (Country) new Transformer(Country.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return country;
    }

    @Override
    public int create(Country entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setShort(1, entity.getId());
            preparedStatement.setString(2, entity.getCode());
            preparedStatement.setString(3, entity.getName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Country entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getCode());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setShort(3, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE)){
            preparedStatement.setInt(1, id);
        return preparedStatement.executeUpdate();
        }
    }
}
