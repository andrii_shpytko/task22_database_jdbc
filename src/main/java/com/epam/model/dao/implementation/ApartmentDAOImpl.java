package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.ApartmentDAO;
import com.epam.model.entity.Apartment;
import com.epam.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ApartmentDAOImpl implements ApartmentDAO {
    private static final String FIND_ALL = "SELECT * FROM apartment";
    private static final String FIND_BY_ID = "SELECT * FROM apartment WHERE id=?";
    private static final String FIND_BY_CLASS_TYPE = "SELECT * FROM apartment WHERE class_type=?";
    private static final String DELETE = "DELETE FROM apartment WHERE id=?";
    private static final String CREATE = "INSERT INTO apartment " +
            "(id, quantity_rooms, quantity_seats, class_type, hotel_id) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE apartment SET quantity_rooms=?, quantity_seats=?, class_type=? " +
            "WHERE id=?";

    @Override
    public List<Apartment> findByClassType(String classType) throws SQLException {
        List<Apartment> entity = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_CLASS_TYPE)){
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    entity.add( (Apartment) new Transformer(Apartment.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return entity;
    }

    @Override
    public List<Apartment> findAll() throws SQLException {
        List<Apartment> apartments = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    apartments.add((Apartment) new Transformer(Apartment.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return apartments;
    }

    @Override
    public Apartment findById(Number id) throws SQLException {
        Apartment entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)) {
            preparedStatement.setShort(1, (Short) id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Apartment) new Transformer(Apartment.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Apartment entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setShort(1, entity.getId());
            preparedStatement.setShort(2, entity.getQuantityRooms());
            preparedStatement.setShort(3, entity.getQuantitySeats());
            preparedStatement.setString(4, String.valueOf(entity.getApartmentClass()));
            preparedStatement.setInt(5, entity.getHotelId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Apartment entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setShort(1, entity.getQuantityRooms());
            preparedStatement.setShort(2, entity.getQuantitySeats());
            preparedStatement.setString(3, String.valueOf(entity.getApartmentClass()));
            preparedStatement.setShort(4, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
