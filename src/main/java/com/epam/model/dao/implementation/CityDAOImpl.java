package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.CityDAO;
import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.City;
import com.epam.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CityDAOImpl implements CityDAO {
    private static final String FIND_ALL = "SELECT * FROM city";
    private static final String FIND_BY_ID = "SELECT * FROM city WHERE id=?";
    private static final String DELETE = "DELETE FROM city WHERE id=?";
    private static final String CREATE = "INSERT INTO city " +
            "(id, name, country_id) VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE city SET name=?, country_id=? " +
            "WHERE id=?";

    @Override
    public List<City> findAll() throws SQLException {
        List<City> cities = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    cities.add((City) new Transformer(City.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return cities;
    }

    @Override
    public City findById(Number id) throws SQLException {
        City entity = null;
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(FIND_BY_ID)){
            preparedStatement.setShort(1, (Short) id);
            try (ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                   entity = (City) new Transformer(City.class).fromResultSetToEntity(resultSet);
                   break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(City entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setShort(3, entity.getCountryId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(City entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setShort(2, entity.getCountryId());
            preparedStatement.setShort(3, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
