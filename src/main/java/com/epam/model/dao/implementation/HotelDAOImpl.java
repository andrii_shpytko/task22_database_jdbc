package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.HotelDAO;
import com.epam.model.entity.Hotel;
import com.epam.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HotelDAOImpl implements HotelDAO {
    private static final String FIND_ALL = "SELECT * FROM hotel";
    private static final String FIND_BY_ID = "SELECT * FROM hotel WHERE id=?";
    private static final String DELETE = "DELETE FROM hotel WHERE id=?";
    private static final String CREATE = "INSERT INTO hotel " +
            "(id, name, quantity_apartments, star, street, numbBuilding, hotel_network_id, city_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE hotel SET " +
            "name=?, quantity_apartments=?, star=?, street=?, numbBuilding=?, hotel_network_id=?, city_id=? " +
            "WHERE id=?";

    @Override
    public List<Hotel> findAll() throws SQLException {
        List<Hotel> hotels = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    hotels.add((Hotel) new Transformer(Hotel.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return hotels;
    }

    @Override
    public Hotel findById(Number id) throws SQLException {
        Hotel entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)) {
            preparedStatement.setShort(1, (Short) id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Hotel) new Transformer(Hotel.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Hotel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setShort(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setShort(3, entity.getQuantityApartments());
            preparedStatement.setString(4, String.valueOf(entity.getHotelStar().getStar()));
            preparedStatement.setString(5, entity.getStreet());
            preparedStatement.setString(6, entity.getNumbBuilding());
            preparedStatement.setShort(7, entity.getHotelNetworkId());
            preparedStatement.setShort(8, entity.getCityId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Hotel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setShort(2, entity.getQuantityApartments());
            preparedStatement.setString(3, entity.getHotelStar().getStar());
            preparedStatement.setString(4, entity.getStreet());
            preparedStatement.setString(5, entity.getNumbBuilding());
            preparedStatement.setShort(6, entity.getHotelNetworkId());
            preparedStatement.setShort(7, entity.getCityId());
            preparedStatement.setShort(8, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
