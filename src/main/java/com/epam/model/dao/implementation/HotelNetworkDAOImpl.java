package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.HotelNetworkDAO;
import com.epam.model.entity.HotelNetwork;
import com.epam.model.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class HotelNetworkDAOImpl implements HotelNetworkDAO {
    private static final String FIND_ALL = "SELECT * FROM hotel_network";
    private static final String FIND_BY_ID = "SELECT * FROM hotel_network WHERE id=?";
    private static final String DELETE = "DELETE FROM hotel_network WHERE id=?";
    private static final String CREATE = "INSERT INTO hotel_network " +
            "(id, name) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE hotel_network SET name=?" +
            "WHERE id=?";

    @Override
    public List<HotelNetwork> findAll() throws SQLException {
        List<HotelNetwork> hotelNetworks = new ArrayList<>();
        try (Statement statement = ConnectionManager.getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    hotelNetworks.add((HotelNetwork) new Transformer(HotelNetwork.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return hotelNetworks;
    }

    @Override
    public HotelNetwork findById(Number id) throws SQLException {
        HotelNetwork hotelNetwork = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(FIND_BY_ID)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    hotelNetwork = (HotelNetwork) new Transformer(HotelNetwork.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return hotelNetwork;
    }

    @Override
    public int create(HotelNetwork entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(CREATE)) {
            preparedStatement.setShort(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(HotelNetwork entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setShort(2, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
