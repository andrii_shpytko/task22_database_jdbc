package com.epam.model.dao.implementation;

import com.epam.model.connectionManager.ConnectionManager;
import com.epam.model.dao.ReservationDAO;
import com.epam.model.entity.Reservation;
import com.epam.model.transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReservationDAOImpl implements ReservationDAO {
    private static final String FIND_ALL = "SELECT * FROM reservation";
    private static final String FIND_BY_ID = "SELECT * FROM reservation WHERE id=?";
    private static final String DELETE = "DELETE FROM reservation WHERE id=?";
    private static final String CREATE = "INSERT INTO reservation " +
            "(id, start_date, finish_date, apartment_id, user_id) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE reservation SET start_date=?, finish_date=?, apartment_id=?, user_id=? " +
            "WHERE id=?";

    @Override
    public List<Reservation> findAll() throws SQLException {
        List<Reservation> reservations = new ArrayList<>();
        try (Statement statement = ConnectionManager.getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    reservations.add((Reservation) new Transformer(Reservation.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return reservations;
    }

    @Override
    public Reservation findById(Number number) throws SQLException {
        Reservation reservation = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(FIND_BY_ID)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    reservation = (Reservation) new Transformer(Reservation.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return reservation;
    }

    @Override
    public int create(Reservation entity) throws SQLException {
        //todo this method!!!
        return 0;
    }

    @Override
    public int update(Reservation entity) throws SQLException {
        //todo this method!!!
        return 0;
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection()
                .prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
