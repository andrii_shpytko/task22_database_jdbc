package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.Review;

public interface ReviewDAO extends GeneralDAO<Review, Integer> {
}
