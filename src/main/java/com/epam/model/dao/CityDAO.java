package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.City;

public interface CityDAO extends GeneralDAO<City, Integer> {
}
