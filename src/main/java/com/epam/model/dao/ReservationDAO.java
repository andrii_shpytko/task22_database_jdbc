package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.Reservation;

public interface ReservationDAO extends GeneralDAO<Reservation, Integer> {
}
