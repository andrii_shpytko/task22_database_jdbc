package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.User;

public interface UserDAO extends GeneralDAO<User, Integer> {
}
