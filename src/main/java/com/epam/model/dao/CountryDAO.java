package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.Country;

import java.sql.SQLException;

public interface CountryDAO extends GeneralDAO<Country, Integer> {
    Country findByName(String name) throws SQLException;
}
