package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.Hotel;

public interface HotelDAO extends GeneralDAO<Hotel, Integer> {
}
