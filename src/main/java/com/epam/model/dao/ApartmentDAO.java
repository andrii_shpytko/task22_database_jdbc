package com.epam.model.dao;

import com.epam.model.dao.general.GeneralDAO;
import com.epam.model.entity.Apartment;

import java.sql.SQLException;
import java.util.List;

public interface ApartmentDAO extends GeneralDAO<Apartment, Integer> {
    List<Apartment> findByClassType(String classType) throws SQLException;

}
