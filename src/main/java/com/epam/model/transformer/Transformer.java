package com.epam.model.transformer;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKeyKonposite;
import com.epam.model.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet resultSet) throws SQLException {
        Object entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    // if field is annotated with @Column
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        int length = column.length();
                        Class fieldType = field.getType();
                        if (fieldType == String.class) {
                            field.set(entity, resultSet.getString(name));
                        } else if (fieldType == Integer.class) {
                            field.set(entity, resultSet.getInt(name));
                        } else if (fieldType == LocalDateTime.class) {
                            field.set(entity, resultSet.getDate(name));
                        } else if (fieldType == Byte.class) {
                            field.set(entity, resultSet.getByte(name));
                        } else if(fieldType == Short.class){
                            field.set(entity, resultSet.getShort(name));
                        }
                    } // if field is annotated with @PrimaryKeyComposite
                    else if (field.isAnnotationPresent(PrimaryKeyKonposite.class)) {
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        Object fk = fieldType.getConstructor().newInstance();
                        field.set(entity, fk);
                        Field[] fieldsInner = fieldType.getDeclaredFields();
                        for (Field fieldInner : fieldsInner){
                            if(fieldInner.isAnnotationPresent(Column.class)){
                                Column column = fieldInner.getAnnotation(Column.class);
                                String name = column.name();
                                int length = column.length();
                                fieldInner.setAccessible(true);
                                Class fieldInnerType = fieldInner.getType();
                                if(fieldInnerType == String.class){
                                    fieldInner.set(fk,resultSet.getString(name));
                                } else if(fieldInnerType == Integer.class){
                                    fieldInner.set(fk, resultSet.getInt(name));
                                } else if(fieldInnerType == Date.class){
                                    fieldInner.set(fk, resultSet.getDate(name));
                                } else if(fieldInnerType == Byte.class){
                                    fieldInner.set(fk, resultSet.getByte(name));
                                } else if(fieldInnerType == Short.class){
                                    fieldInner.set(fk,resultSet.getShort(name));
                                }
                            }
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
