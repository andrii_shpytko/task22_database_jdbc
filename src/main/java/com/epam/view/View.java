package com.epam.view;

import com.epam.model.entity.*;
import com.epam.model.entity.entityType.ApartmentClass;
import com.epam.model.entity.entityType.Gender;
import com.epam.model.entity.entityType.Star;
import com.epam.model.sevices.*;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("A", "\tA\tSelect all tables");
        menu.put("B", "\tB\tSelect structure of DB");
        menu.put("Q", "\tQ\tExit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("A", () -> selectAllTable());
    }

    private void selectAllTable() throws SQLException {
        System.out.println("Hi");
        selectCountry();
//        selectCity();
//        selectHotelNetwork();
//        selectHotel();
//        selectApartment();
//        selectUser();
//        selectReservation();
//        selectReview();
    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    private void outputSubMenu(String text) {
        System.out.println("\nSubMenu:");
        for (String key : menu.keySet()) {
            if (key.length() != 1 && key.substring(0, 1).equals(text)) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu = null;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();
            }
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!"Q".equals(keyMenu));
    }

    //-----------------------------------------------------
    private void selectCountry() throws SQLException {
        System.out.println("\nTable: Country");
        CountryService countryService = new CountryService();
        List<Country> countries = countryService.findAll();
        for (Country entity : countries) {
            System.out.println(entity);
        }
    }

    private void deleteFromCountry() throws SQLException {
        System.out.print("Input ID for Country: ");
        Integer id = input.nextInt();
        input.nextLine();
        CountryService countryService = new CountryService();
        int count = countryService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);

    }

    private void findCountryById() throws SQLException {
        System.out.print("Input ID for Country: ");
        Integer id = input.nextInt();
        input.nextLine();
        CountryService countryService = new CountryService();
        Country entity = countryService.findById(id);
        System.out.println(entity);
    }

    private void findCountryByName() throws SQLException {
        System.out.print("Input ID for Country: ");
        String name = input.nextLine();
        CountryService countryService = new CountryService();
        Country entity = countryService.findByName(name);
        System.out.println(entity);
    }

    private void createForCountry() throws SQLException {
        System.out.print("Input ID for Country: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input code name for Country: ");
        String nameCode = input.nextLine();
        System.out.print("Input name for Country: ");
        String name = input.nextLine();
        Country entity = new Country(id, nameCode, name);
        CountryService countryService = new CountryService();
        int count = countryService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateForCountry() throws SQLException {
        System.out.print("Input ID for Country: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input code name for Country: ");
        String nameCode = input.nextLine();
        System.out.print("Input name for Country: ");
        String name = input.nextLine();
        Country entity = new Country(id, nameCode, name);
        CountryService countryService = new CountryService();
        int count = countryService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectCity() throws SQLException {
        System.out.println("\nTable: City");
        CityService cityService = new CityService();
        List<City> cities = cityService.findAll();
        for (City entity : cities) {
            System.out.println(entity);
        }
    }

    private void findCityById() throws SQLException {
        System.out.print("Input ID for City: ");
        Integer id = input.nextInt();
        input.nextLine();
        CityService cityService = new CityService();
        City entity = cityService.findById(id);
        System.out.println(entity);
    }

    private void createForCity() throws SQLException {
        System.out.print("Input ID for City: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input name for City: ");
        String name = input.nextLine();
        System.out.print("Input ID for Country: ");
        Short countryId = input.nextShort();
        City entity = new City(id, name, countryId);
        CityService cityService = new CityService();
        int count = cityService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateForCity() throws SQLException {
        System.out.print("Input ID for City: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input name for City: ");
        String name = input.nextLine();
        System.out.print("Input ID for Country: ");
        Short countryId = input.nextShort();
        City entity = new City(id, name, countryId);
        CityService cityService = new CityService();
        int count = cityService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteFromCity() throws SQLException {
        System.out.print("Input ID for City: ");
        Integer id = input.nextInt();
        input.nextLine();
        CityService cityService = new CityService();
        int count = cityService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectHotelNetwork() throws SQLException {
        System.out.println("\nTable: Hotel network");
        HotelNetworkService hotelNetworkService = new HotelNetworkService();
        List<HotelNetwork> hotelNetworks = hotelNetworkService.findAll();
        for (HotelNetwork entity : hotelNetworks) {
            System.out.println(entity);
        }
    }

    private void findHotelNetworkById() throws SQLException {
        System.out.print("Input ID for Hotel network: ");
        Integer id = input.nextInt();
        input.nextLine();
        HotelNetworkService hotelNetworkService = new HotelNetworkService();
        HotelNetwork entity = hotelNetworkService.findById(id);
        System.out.println(entity);
    }

    private void updateForHotelNetwork() throws SQLException {
        System.out.print("Input ID for Hotel Network: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input name for Hotel Network: ");
        String name = input.nextLine();
        HotelNetwork entity = new HotelNetwork(id, name);
        HotelNetworkService hotelNetworkService = new HotelNetworkService();
        int count = hotelNetworkService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteFromHotelNetwork() throws SQLException {
        System.out.print("Input ID for City: ");
        Integer id = input.nextInt();
        input.nextLine();
        HotelNetworkService hotelNetworkService = new HotelNetworkService();
        int count = hotelNetworkService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForHotelNetwork() throws SQLException {
        System.out.print("Input ID for Hotel Network: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input name for Hotel Network: ");
        String name = input.nextLine();
        HotelNetwork entity = new HotelNetwork(id, name);
        HotelNetworkService hotelNetworkService = new HotelNetworkService();
        int count = hotelNetworkService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectHotel() throws SQLException {
        System.out.println("\nTable: Hotel");
        HotelService hotelService = new HotelService();
        List<Hotel> hotels = hotelService.findAll();
        for (Hotel entity : hotels) {
            System.out.println(entity);
        }
    }

    private void findHotelById() throws SQLException {
        System.out.print("Input ID for Hotel: ");
        Integer id = input.nextInt();
        input.nextLine();
        HotelService hotelService = new HotelService();
        Hotel entity = hotelService.findById(id);
        System.out.println(entity);
    }

    private void updateForHotel() throws SQLException {
        System.out.print("Input ID for Hotel: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input name for Hotel: ");
        String name = input.nextLine();
        System.out.print("Input quantity apartment for Hotel: ");
        Short quantityApartment = input.nextShort();
        input.nextLine();
        System.out.print("Input quantity star for Hotel [3 or 4 or 5]: ");
        String nameStar = input.nextLine();
        System.out.print("Input name street for Hotel: ");
        String street = input.nextLine();
        System.out.print("Input № building for Hotel: ");
        String numbBuilding = input.nextLine();
        System.out.print("Input ID for Hotel Network: ");
        Short idHotelNetwork = input.nextShort();
        input.nextLine();
        System.out.print("Input ID for City: ");
        Short idCity = input.nextShort();
        input.nextLine();
        Hotel entity = new Hotel(id, name, quantityApartment, Star.valueOf(nameStar),
                street, numbBuilding, idHotelNetwork, idCity);
        HotelService hotelService = new HotelService();
        int count = hotelService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteFromHotel() throws SQLException {
        System.out.print("Input ID for City: ");
        Integer id = input.nextInt();
        input.nextLine();
        HotelService hotelService = new HotelService();
        int count = hotelService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForHotel() throws SQLException {
        System.out.print("Input ID for Hotel: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input name for Hotel: ");
        String name = input.nextLine();
        System.out.print("Input quantity apartment for Hotel: ");
        Short quantityApartment = input.nextShort();
        input.nextLine();
        System.out.print("Input quantity star for Hotel [3 or 4 or 5]: ");
        String nameStar = input.nextLine();
        System.out.print("Input name street for Hotel: ");
        String street = input.nextLine();
        System.out.print("Input № building for Hotel: ");
        String numbBuilding = input.nextLine();
        System.out.print("Input ID for Hotel Network: ");
        Short idHotelNetwork = input.nextShort();
        input.nextLine();
        System.out.print("Input ID for City: ");
        Short idCity = input.nextShort();
        input.nextLine();
        Hotel entity = new Hotel(id, name, quantityApartment, Star.valueOf(nameStar),
                street, numbBuilding, idHotelNetwork, idCity);
        HotelService hotelService = new HotelService();
        int count = hotelService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectApartment() throws SQLException {
        System.out.println("\nTable: Apartment");
        ApartmentService apartmentService = new ApartmentService();
        List<Apartment> apartments = apartmentService.findAll();
        for (Apartment entity : apartments) {
            System.out.println(entity);
        }
    }

    private void findApartmentByClassType() throws SQLException {
        System.out.print("Input class for Apartment [luxury or standard or economy]");
        String classType = input.nextLine();
        ApartmentService apartmentService = new ApartmentService();
        apartmentService.findByClassType(classType);
    }

    private void findApartmentById() throws SQLException {
        System.out.print("Input ID for Apartment: ");
        Integer id = input.nextInt();
        input.nextLine();
        ApartmentService apartmentService = new ApartmentService();
        Apartment entity = apartmentService.findById(id);
        System.out.println(entity);
    }

    private void createForApartment() throws SQLException {
        System.out.print("Input ID for Apartment: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input quantity rooms for Apartment: ");
        Byte quantityRooms = input.nextByte();
        input.nextLine();
        System.out.print("Input quantity seats in Apartment: ");
        Byte quantitySeats = input.nextByte();
        input.nextLine();
        System.out.print("Input class for Apartment [luxury or standard or economy]");
        String classType = input.nextLine();
        System.out.print("Input ID for Hotel: ");
        Short hotelId = input.nextShort();
        input.nextLine();
        Apartment entity = new Apartment(id, quantityRooms, quantitySeats,
                ApartmentClass.valueOf(classType), hotelId);
        ApartmentService apartmentService = new ApartmentService();
        int count = apartmentService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateForApartment() throws SQLException {
        System.out.print("Input ID for Apartment: ");
        Short id = input.nextShort();
        input.nextLine();
        System.out.print("Input quantity rooms for Apartment: ");
        Byte quantityRooms = input.nextByte();
        input.nextLine();
        System.out.print("Input quantity seats in Apartment: ");
        Byte quantitySeats = input.nextByte();
        input.nextLine();
        System.out.print("Input class for Apartment [luxury or standard or economy]");
        String classType = input.nextLine();
        System.out.print("Input ID for Hotel: ");
        Short hotelId = input.nextShort();
        input.nextLine();
        ApartmentService apartmentService = new ApartmentService();
        Apartment entity = new Apartment(id, quantityRooms, quantitySeats,
                ApartmentClass.valueOf(classType), hotelId);
        int count = apartmentService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteForApartment() throws SQLException {
        System.out.print("Input ID for Apartment: ");
        Integer id = input.nextInt();
        input.nextLine();
        ApartmentService apartmentService = new ApartmentService();
        int count = apartmentService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectUser() throws SQLException {
        System.out.println("\nTable: User");
        UserService userService = new UserService();
        List<User> users = userService.findAll();
        for (User entity : users) {
            System.out.println(entity);
        }
    }

    private void findUserById() throws SQLException {
        System.out.print("Input ID for User: ");
        Integer id = input.nextInt();
        UserService userService = new UserService();
        User entity = userService.findById(id);
        System.out.println(entity);
    }

    private void createForUser() throws SQLException {
        System.out.print("Input ID for User: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.print("Input name for User: ");
        String name = input.nextLine();
        System.out.print("Input surname for User: ");
        String surname = input.nextLine();
        System.out.print("Input gender type [man or woman]");
        String genderType = input.nextLine();
        System.out.print("Input email for User: ");
        String email = input.nextLine();
        System.out.print("Input phone number for User: ");
        String phoneNumber = input.nextLine();
        System.out.print("Input number bank card for User: ");
        String numberBankCard = input.nextLine();
        UserService userService = new UserService();
        User entity = new User(id, name, surname, Gender.valueOf(genderType), email,
                phoneNumber, numberBankCard);
        int count = userService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateForUser() throws SQLException {
        System.out.print("Input ID for User: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.print("Input name for User: ");
        String name = input.nextLine();
        System.out.print("Input surname for User: ");
        String surname = input.nextLine();
        System.out.print("Input gender type [man or woman]");
        String genderType = input.nextLine();
        System.out.print("Input email for User: ");
        String email = input.nextLine();
        System.out.print("Input phone number for User: ");
        String phoneNumber = input.nextLine();
        System.out.print("Input number bank card for User: ");
        String numberBankCard = input.nextLine();
        UserService userService = new UserService();
        User entity = new User(id, name, surname, Gender.valueOf(genderType), email,
                phoneNumber, numberBankCard);
        int count = userService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteForUser() throws SQLException {
        System.out.print("Input ID for User: ");
        Integer id = input.nextInt();
        input.nextLine();
        UserService userService = new UserService();
        int count = userService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectReservation() throws SQLException {
        System.out.println("\tTable: Reservation");
        ReservationService reservationService = new ReservationService();
        List<Reservation> reservations = reservationService.findAll();
        for (Reservation entity : reservations) {
            System.out.println(entity);
        }
    }

    private void findReservationById() throws SQLException {
        System.out.print("Input ID for Reservation: ");
        Integer id = input.nextInt();
        input.nextLine();
        ReservationService reservationService = new ReservationService();
        Reservation entity = reservationService.findById(id);
        System.out.println(entity);
    }

    private void createForReservation() throws SQLException {
        System.out.print("Input ID for Reservation: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.print("Input start Date for Reservation [yyyy-MM-dd HH:mm] for example [2019-08-22 12:30]: ");
        String startDate = input.nextLine();
        LocalDateTime start = formatterOfDate(startDate);
        System.out.print("Input finish Date for Reservation [yyyy-MM-dd HH:mm] for example [2019-08-22 12:30]: ");
        String finishDate = input.nextLine();
        LocalDateTime finish = formatterOfDate(finishDate);
        System.out.print("Input apartment ID: ");
        Short apartmentId = input.nextShort();
        input.nextLine();
        System.out.print("Input user ID: ");
        Integer userId = input.nextInt();
        input.nextLine();
        ReservationService reservationService = new ReservationService();
        Reservation entity = new Reservation(id, start, finish, apartmentId, userId);
        int count = reservationService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateForReservation() throws SQLException {
        System.out.print("Input ID for Reservation: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.print("Input start Date for Reservation [yyyy-MM-dd HH:mm] for example [2019-08-22 12:30]: ");
        String startDate = input.nextLine();
        LocalDateTime start = formatterOfDate(startDate);
        System.out.print("Input finish Date for Reservation [yyyy-MM-dd HH:mm] for example [2019-08-22 12:30]: ");
        String finishDate = input.nextLine();
        LocalDateTime finish = formatterOfDate(finishDate);
        System.out.print("Input apartment ID: ");
        Short apartmentId = input.nextShort();
        input.nextLine();
        System.out.print("Input user ID: ");
        Integer userId = input.nextInt();
        input.nextLine();
        ReservationService reservationService = new ReservationService();
        Reservation entity = new Reservation(id, start, finish, apartmentId, userId);
        int count = reservationService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteForReservation() throws SQLException {
        System.out.print("Input ID for Reservation: ");
        Integer id = input.nextInt();
        input.nextLine();
        ReservationService reservationService = new ReservationService();
        int count = reservationService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //-----------------------------------------------------
    private void selectReview() throws SQLException {
        System.out.println("\nTable: Review");
        ReviewService reviewService = new ReviewService();
        List<Review> reviews = reviewService.findAll();
        for (Review entity : reviews) {
            System.out.println(entity);
        }
    }

    private void findReviewById() throws SQLException {
        System.out.print("Input ID for Review: ");
        Integer id = input.nextInt();
        input.nextLine();
        ReviewService reviewService = new ReviewService();
        Review entity = reviewService.findById(id);
        System.out.println(entity);
    }

    private void createForReview() throws SQLException {
        System.out.print("Input ID for Review: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.print("Input apartment ID: ");
        Short apartmentId = input.nextShort();
        input.nextLine();
        System.out.print("Input user ID: ");
        Integer userId = input.nextInt();
        input.nextLine();
        Review entity = new Review(id, apartmentId, userId);
        ReviewService reviewService = new ReviewService();
        int count = reviewService.create(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void updateForReview() throws SQLException {
        System.out.print("Input ID for Review: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.print("Input apartment ID: ");
        Short apartmentId = input.nextShort();
        input.nextLine();
        System.out.print("Input user ID: ");
        Integer userId = input.nextInt();
        input.nextLine();
        Review entity = new Review(id, apartmentId, userId);
        ReviewService reviewService = new ReviewService();
        reviewService.update(entity);
    }

    private void deleteFromReview() throws SQLException {
        System.out.print("Input ID for Review: ");
        Integer id = input.nextInt();
        input.nextLine();
        ReviewService reviewService = new ReviewService();
        int count = reviewService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //-----------------------------------------------------
    private LocalDateTime formatterOfDate(String timeLine) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(timeLine, formatter);
        return dateTime;
    }
}
